% ---------------------------------------------------- %
% Thesis template:                                     %
% Based on PhD Thesis of Dr Gregory J. Sheard (2004)   %
%   Contains code from several third-party sources     %
%   Original authors attributed where possible         %
%   Template prepared for distribution to postgraduate %
%   students November 2005.                            %
% ---------------------------------------------------- %

\documentclass[11pt,titlepage,twoside, openright]{report}
\usepackage[centertags]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amscd}
\usepackage{ifthen}
\usepackage{newlfont}
\usepackage{graphics}
\usepackage{color}
\usepackage{vmargin}
\usepackage{pifont}
\usepackage{epsfig}
\usepackage{afterpage}

\usepackage[mathcal]{euscript}

% Natbib bibliography package
\usepackage{natbib}
\bibpunct{(}{)}{;}{a}{}{,}

% Caption2 package - define custom caption style
\usepackage[small,sc]{caption2}
\setlength{\captionmargin}{5pt}

% Sub figure package for (a), (b) etc
\usepackage{subfigure}

% Supertab package for multi page tables
\usepackage{supertabular}

\setpapersize{A4}
\setmarginsrb{40mm}{30mm}{25mm}{20mm}{0mm}{0mm}{20mm}{10mm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Line spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\defbaselineskip}
\setlength{\defbaselineskip}{\baselineskip}
\newcommand{\setlinespacing}[1]{\setlength{\baselineskip}{#1 \defbaselineskip}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Graphics/Pictures/Images etc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\topfraction}{0.8}
\renewcommand{\floatpagefraction}{0.8}
\renewcommand{\textfraction}{0.2}

\newenvironment{MyFig}
{
    \begin{figure}
         \small
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
         \begin{center}
         \renewcommand{\baselinestretch}{1.25}
}{
         \end{center}
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
    \end{figure}
}

\newenvironment{MyFigHere}
{
    \begin{figure}[ph]
         \small
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
         \begin{center}
         \renewcommand{\baselinestretch}{1.25}
}{
         \end{center}
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
    \end{figure}
}

\newcommand{\MyFigLeft}[1]{
  \afterpage{
    \ifthenelse{
      \isodd{
        \value{page}
      }
    }
    {\afterpage{#1}} % odd page
    {#1} % even page
  }
}

\newcommand{\MyFigRight}[1]{
  \afterpage{
    \ifthenelse{
      \isodd{
        \value{page}
      }
    }
    {#1} % odd page
    {\afterpage{#1}} % even page
  }
}

\newenvironment{MyFigSpread}
{
    \begin{figure}[!ht]
         \small
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
         \begin{center}
         \renewcommand{\baselinestretch}{1.25}
}{
         \end{center}
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
    \end{figure}
}



\newenvironment{MyTable}
{
    \begin{table}
          \small
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
         \begin{center}
         \renewcommand{\baselinestretch}{1.25}
}{
         \end{center}
         \setlinespacing{0}
         \rule{\textwidth}{.2mm}
    \end{table}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure sizing and spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gap between figures 2% of text width approx. 3mm
\newcommand{\FigGap}{\hspace{0.02\textwidth}}
% One figure 70% of text width
\newlength{\WdOneFig}
\setlength{\WdOneFig}{0.70\textwidth}
% One Bifurcation plot 90% of text width
\newlength{\WdOneChrt}
\setlength{\WdOneChrt}{0.90\textwidth}
% Two figures 80% of text width
\newlength{\WdTwoFig}
\setlength{\WdTwoFig}{0.39\textwidth}
% Two charts 95% of text width
\newlength{\WdTwoChrt}
\setlength{\WdTwoChrt}{0.465\textwidth}
% Three figures 90% of text width
\newlength{\WdThreeFig}
\setlength{\WdThreeFig}{0.28667\textwidth}
% Four figures 100% of text width
\newlength{\WdFourFig}
\setlength{\WdFourFig}{0.235\textwidth}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\nmbeqn}[2]{\begin{equation}#1\label{#2}\end{equation}}
\newcommand{\inlneqn}[1]{\begin{displymath}#1\end{displaymath}}
\numberwithin{equation}{chapter}
\renewcommand{\theequation}{\thechapter.\arabic{equation}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math from JFM papers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\BEQ}{\begin{equation}}
\newcommand{\EEQ}{\end{equation}}
\newcommand{\BER}{\begin{eqnarray}}
\newcommand{\EER}{\end{eqnarray}}
\newcommand{\BEQN}{\begin{equation*}}
\newcommand{\EEQN}{\end{equation*}}
\newcommand{\BERN}{\begin{eqnarray*}}
\newcommand{\EERN}{\end{eqnarray*}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Math Symbols (frequently used)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\essnorm}[1]{\norm{#1}_{\text{\rm\normalshape ess}}}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\seq}[1]{\left<#1\right>}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\IM}{\operatorname{Im}}
\newcommand{\Poly}{{\cal{P}}(E)}
\newcommand{\EssD}{{\cal{D}}}
\newcommand{\half}{\frac{1}{2}}

\newcommand{\pde}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\pdesqr}[2]{\frac{\partial^2 #1}{\partial #2^2}}
\newcommand{\de}[2]{\frac{\mathrm{d} #1}{\mathrm{d} #2}}
\newcommand{\desqr}[2]{\frac{\mathrm{d}^2 #1}{\mathrm{d} #2^2}}
\newcommand{\deltadelta}[2]{\frac{\Delta #1}{\Delta #2}}
\newcommand{\deltadeltasqr}[2]{\frac{\Delta^2 #1}{\Delta #2^2}}

\renewcommand{\exp}[1]{\mathrm{e}^{#1}}

\newcommand\tti{\ensuremath{\rightarrow\infty}}
\newcommand\ttz{\ensuremath{\rightarrow 0}}

% Vectors are upright and bold
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\curl}{\mathrm{curl}}
\newcommand{\vortvect}{\mbox{\boldmath$\omega$}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nomenclature
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\Ar{\textit{Ar\/}}   % Aspect ratio
\newcommand\Rey{\textit{Re\/}}  % Reynolds number
\newcommand\St{\textit{St\/}}   % Froude number
\newcommand\Ma{\textit{Ma\/}}   % Mach number
\newcommand\Fr{\textit{Fr\/}}   % Froude number
\newcommand\U{\textit{U\/}}     % Velocity
\newcommand\Uinf{\mathit{U_{\infty}\/}}     % Velocity
\newcommand\D{\textit{D\/}}     % Ring diameter
%\newcommand\d{\textit{d\/}}     % Ring x-sect dia.
\newcommand\etal{\textit{et al.\/}}
\newcommand\ie{i.e.\ }
\newcommand\eg{e.g.\ }
\newcommand\ii{\mbox{i}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Miscellaneous
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\notes}[1]{$\mathbf{\rhd}$\begin{bfseries}#1$\mathbf{\lhd}$\end{bfseries}}

\newcommand{\nonumchapter}[1]{
                    \chapter*{#1}
                     \addcontentsline{toc}{chapter}
             {\protect\numberline{}#1}}

\newcommand{\PrefaceHeading}[1]{
                    \chapter*{#1}}

\newcommand{\nonumsect}[1]{
                    \section*{#1}
                     \addcontentsline{toc}{section}
             {\protect\numberline{}#1}}

\newcommand{\nonumsubsect}[1]{
                    \subsection*{#1}
                     \addcontentsline{toc}{subsection}
             {\protect\numberline{}#1}}

\newcommand{\nonumsubsubsect}[1]{
                    \subsubsection*{#1}
                     \addcontentsline{toc}{subsubsection}
             {\protect\numberline{}#1}}

\newenvironment{nonumlist}{\renewcommand{\labelitemi}{$\bullet$}\begin{itemize}}{\end{itemize}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Quotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\MyQuote}[2]{
\begin{center}
\parbox{0.8\textwidth}{
\begin{flushleft}
    \parbox{0.6\textwidth}{
    \Large\itshape\setlinespacing{2}
%
% #1 is the quotation
% Use \newline for multiple lines
%
    #1}
\end{flushleft}
\begin{flushright}
    \Large\upshape\setlinespacing{3}
%
% #2 is who said the quotation, and when, where, etc.
% Use \newline for multiple lines
%
    #2
\end{flushright}
}
\end{center}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyphenations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hyphenation{axi-sym-met-ric non-axi-sym-met-ric a-sym-met-ric}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of Contents and Section depth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Begin Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\setlinespacing{1.25}
% ------------------------------------------------------------------------

\pagestyle{plain}
\pagenumbering{roman}

\include{Preface/Titlepage}
\include{Preface/Dedications}
\include{Preface/StOrig}
\include{Preface/Quotation}
\include{Preface/Abstract}
\include{Preface/Acknowledgments}
\include{Preface/Publications}
\include{Preface/Nomenclature}
\include{Preface/Tableofcontents}
%
% ------------------------------------------------------------------------
\setlinespacing{1.5} \pagenumbering{arabic}
%
% Roman numbering for introduction equations and figures
\renewcommand{\theequation}{\roman{equation}}
\renewcommand{\thefigure}{\Roman{figure}}
\renewcommand{\thetable}{\Roman{table}}
%
\include{Chapters/Intro}

% Arabic numbering for main thesis equations and figures
\renewcommand{\theequation}{\thechapter.\arabic{equation}}
\renewcommand{\thefigure}{\thechapter.\arabic{figure}}
\renewcommand{\thetable}{\thechapter.\arabic{table}}
%
\include{Chapters/Ch_lit_rev}
\include{Chapters/Ch1}
\include{Chapters/Ch2}
\include{Chapters/Ch3}
\include{Chapters/Conclusions}

% ------------------------------------------------------------------------

%GATHER{Thesis.bib}   % For Gather Purpose Only
%GATHER{Thesis.bbl}   % For Gather Purpose Only
%GATHER{Thesis.log}   % For Gather Purpose Only
\setlinespacing{1.25}
\bibliographystyle{jfm_mod}
\bibliography{Thesis}
\end{document}
% ------------------------------------------------------------------------
